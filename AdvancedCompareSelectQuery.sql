USE sim4sql
IF OBJECT_ID('AutoSelectCompareProc', 'P') IS NOT NULL
DROP PROC AutoSelectCompareProc
GO
CREATE PROC AutoSelectCompareProc
	@RegistrationNumber int,
	@TaskNumber int,
	@SlaveQuery nvarchar(MAX)
AS
BEGIN
	-- Preparing
	IF OBJECT_ID('TmpMasterTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpMasterTable
	END
	IF OBJECT_ID('TmpSlaveTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpSlaveTable
	END
	DECLARE
	@CheckQuery nvarchar(MAX),
	@MasterQuery nvarchar(MAX),
	@ComperableTable nvarchar(MAX),
	@ComperableDataBase nvarchar(MAX),
	@CreateComperableTableString nvarchar(MAX)
	SET @CheckQuery = @SlaveQuery
	SET @MasterQuery = (SELECT [master_query] FROM [dbo].[task] WHERE [task_number] = @TaskNumber)
	SET @ComperableTable = (SELECT [task_table] FROM [dbo].[task] WHERE [task_number] = @TaskNumber)
	SET @ComperableDataBase = (SELECT [reference] FROM [dbo].[auxiliary_db] WHERE [auxiliary_db_index] = (SELECT [auxiliary_db_index] FROM [dbo].[task] WHERE [task_number] = @TaskNumber))
	
	EXEC CreateTableStringGenerator  @ComperableTable, @ComperableDataBase, @CreateComperableTableString OUTPUT

	IF (SELECT [task_type] FROM [dbo].[task] WHERE [task_number] = @TaskNumber) % 2 != 0
	BEGIN				
		EXEC
		('
			USE ' + @ComperableDataBase +
			' BEGIN TRAN TR1 ' +
			'DECLARE @tmp TABLE (' + @CreateComperableTableString + ') ' 
			+ @MasterQuery + 
			'INSERT INTO @tmp SELECT * from ' + @ComperableTable + '
			ROLLBACK TRAN TR1
			USE sim4sql
			SELECT * INTO TmpMasterTable FROM @tmp
		')
		EXEC
		('
			USE ' + @ComperableDataBase +
			' BEGIN TRAN TR1 ' +
			'DECLARE @tmp TABLE (' + @CreateComperableTableString + ') ' 
			+ @SlaveQuery + 
			'INSERT INTO @tmp SELECT * from ' + @ComperableTable + '
			ROLLBACK TRAN TR1
			USE sim4sql
			SELECT * INTO TmpSlaveTable FROM @tmp
		')
		--SELECT * FROM TmpMasterTable
		--SELECT * FROM TmpSlaveTable
		SET @MasterQuery =  N'SELECT * FROM TmpMasterTable'
		SET @CheckQuery = N'SELECT * FROM TmpSlaveTable'
		SET @ComperableDataBase = N'sim4sql'
	END
	-- Autoselect parts
	EXEC ManualSelectCompareProc @MasterQuery, @CheckQuery, @SlaveQuery, @ComperableDataBase, @RegistrationNumber, @TaskNumber
	IF OBJECT_ID('TmpMasterTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpMasterTable
	END
	IF OBJECT_ID('TmpSlaveTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpSlaveTable
	END
END;

GO
-- Test block

USE sim4sql
DECLARE 
	@TestRegistrationNumber int,
	@TestTaskNumber int,
	@TestAkaQueryBox nvarchar(MAX)
SET @TestRegistrationNumber = 2
SET @TestTaskNumber = 8
--SET @TestAkaQueryBox = N'INSERT INTO [dbo].[main_table] ([ID], [Name], [Surname], [Patronymic]) VALUES (3, N''Nikolai'', N''Semenov'', N''Mihailovich'')'
SET @TestAkaQueryBox = N'Select * from main_table where ID = 2'
EXEC AutoSelectCompareProc @TestRegistrationNumber, @TestTaskNumber, @TestAkaQueryBox

