USE sim4sql
-- ManualSelectCompareProc
IF OBJECT_ID('ManualSelectCompareProc', 'P') IS NOT NULL
DROP PROC ManualSelectCompareProc
GO
CREATE PROC ManualSelectCompareProc 
	@MasterQuery nvarchar(MAX), 
	@SlaveQuery nvarchar(MAX),
	@UserQuery nvarchar(MAX),
	@ComperableDB nvarchar(MAX),
	@RegistrationNumber int,
	@TaskNumber int
AS
BEGIN
	IF OBJECT_ID('tempdb..#tmpTab') IS NOT NULL
	BEGIN
		DROP TABLE #tmpTab
	END

	CREATE TABLE #tmpTab
	(
		[ErrorCode] [int] NOT NULL
	);

	BEGIN TRY
		EXEC	
		('	USE ' + @ComperableDB + '
			INSERT #tmpTab ([ErrorCode]) VALUES
			(
				(
					SELECT count(*) AS ErrorState 
					FROM ((' + @MasterQuery + ' EXCEPT ' + @SlaveQuery + ') UNION ALL (' + @SlaveQuery + ' EXCEPT ' + @MasterQuery + ')) AS CompareTable
				)
			)
		')
		IF (Select ErrorCode from #tmpTab) = 0
		BEGIN
			DELETE FROM [dbo].[completed_task]
			WHERE [registration_number] =  @RegistrationNumber  AND [task_number] =  + @TaskNumber 
			INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) 
			VALUES ( @RegistrationNumber, @TaskNumber, @UserQuery, GETDATE(), 1)
		END
		ELSE 
		THROW 1, 'Compare failure', 1;  
	END TRY
	BEGIN CATCH
		INSERT #tmpTab ([ErrorCode]) VALUES (1)
		IF (SELECT [result_achievement] FROM [dbo].[completed_task] WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber) = 0
		OR (SELECT [result_achievement] FROM [dbo].[completed_task] WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber) = NULL 
		BEGIN
			DELETE FROM [dbo].[completed_task]
			WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber
			INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) 
			VALUES (@RegistrationNumber, @TaskNumber, @UserQuery, GETDATE(), 0)
		END
	END CATCH
	Select TOP(1) MAX(ErrorCode) from #tmpTab
END
GO

-- Test block
USE sim4sql
DECLARE 
	@Query1 nvarchar(MAX), 
	@Query2 nvarchar(MAX),
	@ComperableDB nvarchar(MAX),
	@RegistrationNumber int,
	@TaskNumber int
SET @Query1 = 'SELECT * FROM main_table where ID=1'
SET @Query2 = 'SELECT * FROM main_table where [Name]=''Ivan'''
SET @ComperableDB = 'db_for_lazarus'
SET @RegistrationNumber = 2
SET @TaskNumber = 2

EXEC ManualSelectCompareProc @Query1, @Query2, @Query2, @ComperableDB, @RegistrationNumber, @TaskNumber
