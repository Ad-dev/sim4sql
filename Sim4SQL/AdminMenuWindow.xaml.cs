﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sim4SQL
{
    public partial class AdminMenuWindow : Window
    {
        bool DeathFlag = true;
        string UserId;
        public AdminMenuWindow(string id)
        {
            UserId = id;
            InitializeComponent();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DeathFlag)
            {
                Owner.Close();
            }
            else
            {
                Owner.Show();
            }
        }

        private void CreateTask_Click(object sender, RoutedEventArgs e)
        {
            var ctw = new CreateTaskWindow();
            ctw.Owner = this;
            ctw.Height = this.Height;
            ctw.Width = this.Width;
            ctw.Top = this.Top;
            ctw.Left = this.Left;
            ctw.WindowState = this.WindowState;
            this.Hide();
            ctw.Show();
        }

        private void ReviewProgress_Click(object sender, RoutedEventArgs e)
        {
            var apcw = new AdminProgressChoseWindow();
            apcw.Owner = this;
            apcw.Show();
        }

        private void ChangeUser_Click(object sender, RoutedEventArgs e)
        {
            DeathFlag = false;
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
            this.Close();
        }

        private void DirectQuery_Click(object sender, RoutedEventArgs e)
        {
            var dqw = new DirectQueryWindow();
            dqw.Owner = this;
            dqw.Height = this.Height;
            dqw.Width = this.Width;
            dqw.Top = this.Top;
            dqw.Left = this.Left;
            dqw.WindowState = this.WindowState;
            this.Hide();
            dqw.Show();
        }

        private void EditProfile_Click(object sender, RoutedEventArgs e)
        {
            var epw = new EditProfileWindow(UserId);
            epw.Owner = this;
            epw.Height = this.Height;
            epw.Width = this.Width;
            epw.Top = this.Top;
            epw.Left = this.Left;
            epw.WindowState = this.WindowState;
            this.Hide();
            epw.Show();
        }
    }
}
 