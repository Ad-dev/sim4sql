﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class AdminProgressChoseWindow : Window
    {
        public AdminProgressChoseWindow()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Sumbit_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql DECLARE @login nvarchar(MAX) SET @login = '" + LoginBox.Text + "' SELECT registration_number FROM [user] WHERE login = @login";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                
                var pw = new ProgressWindow(_reader[0].ToString());
                pw.Height = this.Height;
                pw.Width = this.Width;
                pw.Top = this.Top;
                pw.Left = this.Left;
                pw.WindowState = this.WindowState;
                pw.Title = "Sim4SQL: " + LoginBox.Text + " progress";
                pw.Show();
                this.Close();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
            }
        }
    }
}
