﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Sim4SQL
{
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
        }
    }
}
