﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class CreateTaskWindow : Window
    {
        public CreateTaskWindow()
        {
            InitializeComponent();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                string MasterSQL = QueryBox.Text.Replace("'", "''");
                string TaskSQL = TaskBox.Text.Replace("'", "''");
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql 
                DECLARE
                    @TaskName nvarchar(50),
                    @TaskTopic nvarchar(50),
                    @TaskType nchar(1),
                    @TaskDescription nvarchar(MAX),
                    @DependetDB int,
                    @MasterQuery nvarchar(MAX),
                    @TaskTable nvarchar(50)
                SET @TaskName = '" + TaskNameBox.Text + "' " +
               "SET @TaskTopic = '" + TaskTopicBox.Text + "' " +
               "SET @TaskType = '" + (ChangeBit.IsChecked == true ? "1" : "0") + "' " +
               "SET @TaskDescription = '" + TaskSQL + "' " +
               "SET @DependetDB = '" + DependetDbInddexBox.Text + "' " +
               "SET @MasterQuery = '" + MasterSQL + "' " +
               "SET @TaskTable = '" + (TaskNameBox.Text.Length == 0 ? "NULL" : TaskNameBox.Text) + "' " +
               "INSERT[dbo].[task]([task_name], [task_topic], [task_type], [task_description], [auxiliary_db_index], [master_query], [task_table]) " +
               "VALUES(@TaskName, @TaskTopic, @TaskType, @TaskDescription, @DependetDB, @MasterQuery, @TaskTable)";
                cmd.ExecuteNonQuery();
                MessageBox.Show("Task '" + TaskNameBox.Text + "' added successful");
                this.Close();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
            }
        }
    }
}
