﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class DirectQueryWindow : Window
    {
        public DirectQueryWindow()
        {
            InitializeComponent();
        }

        private void ChangeBit_Checked(object sender, RoutedEventArgs e)
        {
            Run.IsEnabled = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }

        private void Run_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = QueryBox.Text;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Complete");
                this.Close();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ChangeBit_Unchecked(object sender, RoutedEventArgs e)
        {
            Run.IsEnabled = false;
        }
    }
}
