﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class EditProfileWindow : Window
    {
        string UserID;
        public EditProfileWindow(string id)
        {
            UserID = id;
            InitializeComponent();
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql SELECT * FROM [user] WHERE registration_number = " + UserID;
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                SurnameBox.Text = _reader[1].ToString();
                NameBox.Text = _reader[2].ToString();
                PatronymicBox.Text = _reader[3].ToString();
                PasswordBox.Text = _reader[5].ToString();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }

        private void Sumbit_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            SqlConnection SubConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql 
                                    DECLARE
                                        @Surname nvarchar(30),
                                        @Name nvarchar(30),
                                        @Patronymic nvarchar(30),
                                        @Password nvarchar(15) 

                                        SET @Surname = '" + SurnameBox.Text + "' " +
                                       "SET @Name = '" + NameBox.Text + "' " +
                                       "SET @Patronymic = '" + PatronymicBox.Text + "' " +
                                       "SET @Password = '" + PasswordBox.Text + "' " +
                                   "UPDATE [dbo].[user] SET [surname] = @Surname, [name] = @Name, [patronymic] = @Patronymic, [passwd] = @Password WHERE registration_number = " + UserID;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Done");
                this.Close();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
                SubConnection.Close();
            }
        }
    }
}
