﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void SumbitButton_Click(object sender, RoutedEventArgs e)
        {
            string id = "";
            bool admin = false;
            bool SuccesAuthenticationFlag = false;
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            SqlConnection SubConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql DECLARE @login nvarchar(MAX) SET @login = '" + LoginBox.Text + "' SELECT passwd, registration_number FROM [user] WHERE login = @login";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                if (_reader[0].ToString() == PasswordBox.Password)
                {
                    id = _reader[1].ToString();
                    SubConnection.Open();
                    SqlCommand cmd2 = SubConnection.CreateCommand();
                    cmd2.CommandText = @"use sim4sql SELECT count(*) as expr1 FROM [super_user] WHERE registration_number = " + id;
                    SqlDataReader _reader2 = cmd2.ExecuteReader();
                    _reader2.Read();
                    if (_reader2[0].ToString() == "1")
                        admin = true;
                    SuccesAuthenticationFlag = true;
                }
                else
                {
                    throw new System.InvalidOperationException();
                }
            }
            catch(System.InvalidOperationException)
            {
                MessageBox.Show("Access Denied!", null, MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
                SubConnection.Close();
            }

            if (SuccesAuthenticationFlag)
            {
                if (admin)
                {
                    var amw = new AdminMenuWindow(id);
                    amw.Owner = this;
                    amw.Height = this.Height;
                    amw.Width = this.Width;
                    amw.WindowState = this.WindowState;
                    amw.Show();
                }
                else
                {
                    var umw = new UserMenuWindow(id);
                    umw.Owner = this;
                    umw.Height = this.Height;
                    umw.Width = this.Width;
                    umw.WindowState = this.WindowState;
                    umw.Show();
                }
                this.Hide();
            }  
        }

        private void Registration_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var rw = new RegistrationWindow();
            rw.Owner = this;
            rw.Height = this.Height;
            rw.Width = this.Width;
            rw.WindowState = this.WindowState;
            rw.Show();
            this.Hide();
        }

        private void Registration_MouseEnter(object sender, MouseEventArgs e)
        {
            RegistrationLabel.Foreground = System.Windows.Media.Brushes.BlueViolet;
        }

        private void RegistrationLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            RegistrationLabel.Foreground = System.Windows.Media.Brushes.Blue;
        }
    }
}
