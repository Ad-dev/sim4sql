﻿using System;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Windows;

namespace Sim4SQL
{   
    public partial class App
    {
        public async void QueryToMainDataBase(string QueryString)
        {
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                await MainConnection.OpenAsync();
                SqlDataAdapter myAdapter = new SqlDataAdapter(QueryString, MainConnection);
                DataSet dset = new DataSet();
                int i = myAdapter.Fill(dset);
                //MainDataGrid.ItemsSource = dset.Tables[0].DefaultView;
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
            }

            MainConnection.Dispose();

        }
    }
}