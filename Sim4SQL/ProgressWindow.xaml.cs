﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class ProgressWindow : Window
    {
        string UserId;
        public class column_structure
        {
            public string column0 { get; set; }
            public string column1 { get; set; }
        }
        public ProgressWindow(string id)
        {
            UserId = id;
            InitializeComponent();
            TaskList.Items.Clear();
            GridView grid_view = new GridView();
            TaskList.View = grid_view;
            grid_view.Columns.Add(new GridViewColumn { Header = "Task", DisplayMemberBinding = new Binding("column0") });
            grid_view.Columns.Add(new GridViewColumn { Header = "Result", DisplayMemberBinding = new Binding("column1") });
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            SqlConnection SubConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SubConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql SELECT count(T.task_name) FROM task T WHERE T.task_number = (SELECT T2.task_number FROM completed_task T2 WHERE registration_number = " + UserId + " AND T.task_number = T2.task_number)";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                int CountOfTopics = Convert.ToInt32(_reader[0].ToString());
                _reader.Close();

                cmd.CommandText = @"use sim4sql SELECT T.task_name, T.task_number FROM task T WHERE T.task_number = (SELECT T2.task_number FROM completed_task T2 WHERE registration_number = " + UserId + " AND T.task_number = T2.task_number)";
                _reader = cmd.ExecuteReader();
                for (int i = 0; i < CountOfTopics; i++)
                {
                    _reader.Read();
                    SqlCommand cmd2 = SubConnection.CreateCommand();
                    cmd2.CommandText = @"use sim4sql SELECT result_achievement FROM completed_task WHERE registration_number = " + UserId + " AND task_number = " + _reader[1].ToString();
                    SqlDataReader _reader2 = cmd2.ExecuteReader();
                    _reader2.Read();

                    TaskList.Items.Add(new column_structure { column0 = _reader[0].ToString(), column1 = (_reader2[0].ToString() == "1" ? "OK" : "NO") });
                    _reader2.Close();
                }
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            finally
            {
                MainConnection.Close();
                SubConnection.Close();
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Height = this.Height;
                Owner.Width = this.Width;
                Owner.Top = this.Top;
                Owner.Left = this.Left;
                Owner.WindowState = this.WindowState;
                Owner.Show();
            }
        }
    }
}
