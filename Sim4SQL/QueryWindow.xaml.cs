﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class QueryWindow : Window
    {
        string UserId;
        string TaskName;
        string TaskNumber;
        public QueryWindow(string id, string task_name, string task_number)
        {
            UserId = id;
            TaskName = task_name;
            TaskNumber = task_number;
            InitializeComponent();
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql SELECT task_description FROM [task] WHERE task_name = '" + TaskName + "'";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                TaskBlock.Text = _reader[0].ToString();
                _reader.Close();
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            finally
            {
                MainConnection.Close();
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            if (QueryBox.Text.Length != 0)
            {
                SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
                try
                {
                    string sql = QueryBox.Text.Replace("'", "''");
                    MessageBox.Show(sql);
                    MainConnection.Open();
                    SqlCommand cmd = MainConnection.CreateCommand();
                    cmd.CommandText = @"USE [sim4sql] 
                                    DECLARE 
                                    @MasterQuery nvarchar(MAX), 
                                    @QueryBoxContent nvarchar(MAX) " +
                                       "SET @QueryBoxContent = '" + sql + "'; " +
                                       "EXEC AutoSelectCompareProc " + UserId + ", " + TaskNumber + ", @QueryBoxContent";
                    SqlDataReader _reader = cmd.ExecuteReader();
                    _reader.Read();
                    if (_reader[0].ToString() == "0")
                    {
                        MessageBox.Show("Task completed successfully");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Task failed");
                    }
                }
                catch (Exception MainConnectionException)
                {
                    MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    MainConnection.Close();
                }
            }
            else
            {
                MessageBox.Show("Query is empty");
            }
        }
    }
}
