﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace Sim4SQL
{
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }

        private void RegistrationButton_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            SqlConnection SubConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql DECLARE @Login nvarchar(30) SET @Login = '" + LoginBox.Text + "' SELECT count(*) as expr1 FROM [user] WHERE login = @Login";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                if (LoginBox.Text.Length == 0)
                {
                    MessageBox.Show("Empty Login!", null, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (_reader[0].ToString() == "0")
                {
                    SubConnection.Open();
                    SqlCommand cmd2 = SubConnection.CreateCommand();
                    cmd2.CommandText = @"use sim4sql
                                        DECLARE
                                        @Surname nvarchar(30),
                                        @Name nvarchar(30),
                                        @Patronymic nvarchar(30),
                                        @Login nvarchar(30),
                                        @Password nvarchar(15) 

                                        SET @Surname = '" + SurnameBox.Text + "' " +
                                       "SET @Name = '" + NameBox.Text + "' " +
                                       "SET @Patronymic = '" + PatronymicBox.Text + "' " +
                                       "SET @Login = '" + LoginBox.Text + "' " +
                                       "SET @Password = '" + PasswordBox.Text + "' " +
                                       "INSERT [dbo].[user] ([surname], [name], [patronymic], [login], [passwd]) VALUES (@Surname, @Name, @Patronymic, @Login, @Password)";
                    cmd2.ExecuteNonQuery();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Login already used!", null, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MainConnection.Close();
                SubConnection.Close();
            }
        }
    }
}
