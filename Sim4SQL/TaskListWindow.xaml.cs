﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Sim4SQL
{
    public partial class TaskListWindow : Window
    {
        string UserId;
        public TaskListWindow(string id)
        {
            UserId = id;
            InitializeComponent();
            TaskTree.Items.Clear();
            SqlConnection MainConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            SqlConnection SubConnection = new SqlConnection(@"Data Source=localhost\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=sim4sql");
            try
            {
                MainConnection.Open();
                SubConnection.Open();
                SqlCommand cmd = MainConnection.CreateCommand();
                cmd.CommandText = @"use sim4sql SELECT count(*) FROM (SELECT DISTINCT task_topic FROM [task]) AS UnicTopics";
                SqlDataReader _reader = cmd.ExecuteReader();
                _reader.Read();
                int CountOfTopics = Convert.ToInt32(_reader[0].ToString());
                _reader.Close();

                cmd.CommandText = @"use sim4sql SELECT task_topic FROM [task] GROUP BY task_topic";
                _reader = cmd.ExecuteReader();
                for (int i = 0; i < CountOfTopics; i++)
                {
                    _reader.Read();
                    var node = new TreeViewItem();
                    node.Header = _reader[0].ToString();
                    TaskTree.Items.Add(node);

                    SqlCommand cmd2 = SubConnection.CreateCommand();
                    cmd2.CommandText = @"use sim4sql SELECT count(task_name) FROM [task] WHERE task_topic = '" + _reader[0].ToString() + "'";
                    SqlDataReader _reader2 = cmd2.ExecuteReader();
                    _reader2.Read();
                    int CountOfNames = Convert.ToInt32(_reader2[0].ToString());
                    _reader2.Close();

                    cmd2.CommandText = @"use sim4sql SELECT task_name, task_number FROM [task] WHERE task_topic = '" + _reader[0].ToString() + "'";
                    _reader2 = cmd2.ExecuteReader();
                    for (int j = 0; j < CountOfNames; j++)
                    {
                        _reader2.Read();
                        var sub_node = new TreeViewItem();
                        sub_node.Header = _reader2[0].ToString();
                        sub_node.Tag = _reader2[1].ToString();
                        sub_node.MouseUp += node_MouseUp;
                        sub_node.MouseEnter += node_MouseEnter;
                        sub_node.MouseLeave += node_MouseLeave;
                        sub_node.Foreground = System.Windows.Media.Brushes.Blue;
                        node.Items.Add(sub_node);
                    }
                    _reader2.Close();
                }
            }
            catch (Exception MainConnectionException)
            {
                MessageBox.Show("Error:\n" + MainConnectionException.Message, MainConnectionException.Source, MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            finally
            {
                MainConnection.Close();
                SubConnection.Close();
            }            
        }
        private void node_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var node = (TreeViewItem)sender;
            var qw = new QueryWindow(UserId, node.Header.ToString(), node.Tag.ToString());
            qw.Owner = this;
            qw.Height = this.Height;
            qw.Width = this.Width;
            qw.WindowState = this.WindowState;
            this.Hide();
            qw.Show();
        }
        private void node_MouseEnter(object sender, MouseEventArgs e)
        {
            var node = (TreeViewItem)sender;
            node.Foreground = System.Windows.Media.Brushes.BlueViolet;
        }
        private void node_MouseLeave(object sender, MouseEventArgs e)
        {
            var node = (TreeViewItem)sender;
            node.Foreground = System.Windows.Media.Brushes.Blue;
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            Owner.Show();
        }
    }
}
