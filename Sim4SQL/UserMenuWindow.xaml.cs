﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;


namespace Sim4SQL
{
    public partial class UserMenuWindow : Window
    {
        bool DeathFlag = true;
        string UserId;
        public UserMenuWindow(string id)
        {
            UserId = id;
            InitializeComponent();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DeathFlag)
            {
                Owner.Close();
            }
            else
            {
                Owner.Show();
            }
        }

        private void ChangeUser_Click(object sender, RoutedEventArgs e)
        {
            DeathFlag = false;
            Owner.Height = this.Height;
            Owner.Width = this.Width;
            Owner.Top = this.Top;
            Owner.Left = this.Left;
            Owner.WindowState = this.WindowState;
            this.Close();
        }

        private void SelectTask_Click(object sender, RoutedEventArgs e)
        {
            var tlw = new TaskListWindow(UserId);
            tlw.Owner = this;
            tlw.Height = this.Height;
            tlw.Width = this.Width;
            tlw.Top = this.Top;
            tlw.Left = this.Left;
            tlw.WindowState = this.WindowState;
            this.Hide();
            tlw.Show();
        }

        private void ReviewProgress_Click(object sender, RoutedEventArgs e)
        {
            var pw = new ProgressWindow(UserId);
            pw.Owner = this;
            pw.Height = this.Height;
            pw.Width = this.Width;
            pw.Top = this.Top;
            pw.Left = this.Left;
            pw.WindowState = this.WindowState;
            this.Hide();
            pw.Show();
        }

        private void EditProfile_Click(object sender, RoutedEventArgs e)
        {
            var epw = new EditProfileWindow(UserId);
            epw.Owner = this;
            epw.Height = this.Height;
            epw.Width = this.Width;
            epw.Top = this.Top;
            epw.Left = this.Left;
            epw.WindowState = this.WindowState;
            this.Hide();
            epw.Show();
        }
    }
}
