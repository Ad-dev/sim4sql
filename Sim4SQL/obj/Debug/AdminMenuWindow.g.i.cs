﻿#pragma checksum "..\..\AdminMenuWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5C802030D9C41C629AD3DBF4BA73D6BBFEDAA78F"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Sim4SQL;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Sim4SQL {
    
    
    /// <summary>
    /// AdminMenuWindow
    /// </summary>
    public partial class AdminMenuWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\AdminMenuWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CreateTask;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\AdminMenuWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ReviewProgress;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\AdminMenuWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DirectQuery;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\AdminMenuWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditProfile;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\AdminMenuWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ChangeUser;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sim4SQL;component/adminmenuwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AdminMenuWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\AdminMenuWindow.xaml"
            ((Sim4SQL.AdminMenuWindow)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.Window_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            this.CreateTask = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\AdminMenuWindow.xaml"
            this.CreateTask.Click += new System.Windows.RoutedEventHandler(this.CreateTask_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ReviewProgress = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\AdminMenuWindow.xaml"
            this.ReviewProgress.Click += new System.Windows.RoutedEventHandler(this.ReviewProgress_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DirectQuery = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\AdminMenuWindow.xaml"
            this.DirectQuery.Click += new System.Windows.RoutedEventHandler(this.DirectQuery_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.EditProfile = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\AdminMenuWindow.xaml"
            this.EditProfile.Click += new System.Windows.RoutedEventHandler(this.EditProfile_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ChangeUser = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\AdminMenuWindow.xaml"
            this.ChangeUser.Click += new System.Windows.RoutedEventHandler(this.ChangeUser_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

