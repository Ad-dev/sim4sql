USE sim4sql
IF OBJECT_ID('CreateTableStringGenerator', 'P') IS NOT NULL
DROP PROC CreateTableStringGenerator
GO
CREATE PROC CreateTableStringGenerator
	@TableName nvarchar(MAX),
	@DataBase nvarchar(MAX),
	@OutputString nvarchar(MAX) OUTPUT
AS
BEGIN
	DECLARE
		@ExecuteString nvarchar(MAX),
		@ExecuteOutput nvarchar(MAX),
		@OputString nvarchar(MAX),
		@ParamsCount int,
		@LoopCounter int
	SET @LoopCounter = 1
	SET @OutputString = N''

	SET @ExecuteString = 'SET @tmp = (SELECT count(c.name) FROM [' + @DataBase + '].[sys].[columns] c WITH(NOLOCK) WHERE c.[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U''))'
	EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp tinyint OUTPUT', @tmp = @ParamsCount OUTPUT;

	WHILE @LoopCounter <= @ParamsCount
	BEGIN
		SET @ExecuteString = 'SET @tmp = (SELECT [name] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
		EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
		SET @OutputString = @OutputString + '[' + @ExecuteOutput + '] '
		SET @ExecuteString = 'SET @tmp = (SELECT [type_name] from (SELECT rank() OVER (ORDER BY [c].[name], [tp].[name]) as [id], [c].[name] , [type_name] = [tp].[name] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) JOIN [' + @DataBase + '].[sys].[types] [tp] WITH(NOLOCK) ON [c].[user_type_id] = [tp].[user_type_id] JOIN [' + @DataBase + '].[sys].[schemas] [s] WITH(NOLOCK) ON [tp].[schema_id] = [s].[schema_id] WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) as [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
		EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
		SET @OutputString = @OutputString +'[' + @ExecuteOutput + ']'
		IF @ExecuteOutput = 'nchar' OR @ExecuteOutput = 'nvarchar' OR @ExecuteOutput = 'char' OR @ExecuteOutput = 'varchar'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [max_length] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[max_length] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			IF Convert(int, @ExecuteOutput) > 0
				SET @ExecuteOutput = Convert(int, @ExecuteOutput) / 2
			ELSE
				SET @ExecuteOutput = 'MAX'
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'binary' OR @ExecuteOutput = 'varbinary'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [max_length] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[max_length] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			IF Convert(int, @ExecuteOutput) < 0
				SET @ExecuteOutput = 'MAX'
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'datetime2' OR @ExecuteOutput = 'datetimeoffset' OR @ExecuteOutput = 'time'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [scale] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[scale] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'numeric' OR @ExecuteOutput = 'decimal'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [precision] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[precision] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ', '

			SET @ExecuteString = 'SET @tmp = (SELECT [scale] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[scale] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + @ExecuteOutput + ')'
		END
		IF @LoopCounter != @ParamsCount
			SET @OutputString = @OutputString + ', '
		SET @LoopCounter = @LoopCounter + 1
	END
	RETURN
END
GO
-- Test block
USE sim4sql
DECLARE 
	@TableName nvarchar(MAX),
	@DataBase nvarchar(MAX),
	@OutStr nvarchar(MAX)
--SET @TableName = 'task'
--SET @DataBase = 'sim4sql'
SET @TableName = 'main_table'
SET @DataBase = 'db_for_lazarus'

EXEC CreateTableStringGenerator  @TableName, @DataBase, @OutStr OUTPUT
print @OutStr
--SELECT c.name , [type_name] = tp.name , type_schema_name = s.name , c.max_length , c.[precision] , c.scale , c.collation_name , c.is_nullable , c.is_identity , seed_value = CASE WHEN c.is_identity = 1 THEN IDENTITYPROPERTY(c.[object_id], 'SeedValue') END , increment_value = CASE WHEN c.is_identity = 1 THEN IDENTITYPROPERTY(c.[object_id], 'IncrementValue') END , computed_definition = OBJECT_DEFINITION(c.[object_id], c.column_id) , default_definition = OBJECT_DEFINITION(c.default_object_id) FROM sys.columns c WITH(NOLOCK) JOIN sys.types tp WITH(NOLOCK) ON c.user_type_id = tp.user_type_id JOIN sys.schemas s WITH(NOLOCK) ON tp.[schema_id] = s.[schema_id] WHERE c.[object_id] = OBJECT_ID('dbo.task', 'U')  

--DECLARE @tmp TABLE ([ID] [int], [Name] [nvarchar](50), [Patronymic] [nvarchar](50), [Surname] [nvarchar](50))
--INSERT INTO @tmp SELECT * from db_for_lazarus.dbo.main_table
