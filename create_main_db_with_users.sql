USE [master]
GO
IF db_id('sim4sql') is not null
DROP DATABASE [sim4sql]
GO
/****** Object:  Database [sim4sql]    Script Date: 14.06.2019 3:12:28 ******/
CREATE DATABASE [sim4sql]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sim4sql', FILENAME = N'A:\data_base\sim4sql_main\sim4sql.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'sim4sql_log', FILENAME = N'A:\data_base\sim4sql_main\sim4sql_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [sim4sql] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sim4sql].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sim4sql] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [sim4sql] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [sim4sql] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [sim4sql] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [sim4sql] SET ARITHABORT OFF 
GO
ALTER DATABASE [sim4sql] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [sim4sql] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [sim4sql] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [sim4sql] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [sim4sql] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [sim4sql] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [sim4sql] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [sim4sql] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [sim4sql] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [sim4sql] SET  DISABLE_BROKER 
GO
ALTER DATABASE [sim4sql] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [sim4sql] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [sim4sql] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [sim4sql] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [sim4sql] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [sim4sql] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [sim4sql] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [sim4sql] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [sim4sql] SET  MULTI_USER 
GO
ALTER DATABASE [sim4sql] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [sim4sql] SET DB_CHAINING OFF 
GO
ALTER DATABASE [sim4sql] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [sim4sql] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [sim4sql] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [sim4sql] SET QUERY_STORE = OFF
GO
USE [sim4sql]
GO
/****** Object:  Table [dbo].[auxiliary_db]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auxiliary_db](
	[auxiliary_db_index] [int] NOT NULL,
	[reference] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_auxiliary_db] PRIMARY KEY CLUSTERED 
(
	[auxiliary_db_index] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[completed_task]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[completed_task](
	[registration_number] [int] NOT NULL,
	[task_number] [int] NOT NULL,
	[user_query] [nvarchar](max) NOT NULL,
	[date] [date] NOT NULL,
	[result_achievement] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[super_user]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[super_user](
	[registration_number] [int] NOT NULL,
 CONSTRAINT [PK_super_user] PRIMARY KEY CLUSTERED 
(
	[registration_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[task]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[task](
	[task_number] [int] IDENTITY(1,1) NOT NULL,
	[task_name] [nvarchar](50) NOT NULL,
	[task_topic] [nvarchar](50) NOT NULL,
	[task_type] [nchar](1) NOT NULL,
	[task_description] [nvarchar](max) NOT NULL,
	[auxiliary_db_index] [int] NOT NULL,
	[master_query] [nvarchar](max) NOT NULL,
	[task_table] [nvarchar](50) NULL,
 CONSTRAINT [PK_task] PRIMARY KEY CLUSTERED 
(
	[task_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[registration_number] [int] IDENTITY(1,1) NOT NULL,
	[surname] [nvarchar](30) NOT NULL,
	[name] [nvarchar](30) NOT NULL,
	[patronymic] [nvarchar](30) NULL,
	[login] [nvarchar](30) NOT NULL,
	[passwd] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[registration_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[auxiliary_db] ([auxiliary_db_index], [reference]) VALUES (1, N'nosql_market')
INSERT [dbo].[auxiliary_db] ([auxiliary_db_index], [reference]) VALUES (2, N'db_for_lazarus')
INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) VALUES (1, 1, N'use nosql_market SELECT [key], value FROM OPENJSON((SELECT json_col FROM dbo.items WHERE article = ''''0000000001''''))', CAST(N'2019-01-31' AS Date), 1)
INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) VALUES (2, 2, N'SELECT * FROM main_table where [Name]=''Ivan''', CAST(N'2019-06-14' AS Date), 1)
INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) VALUES (2, 1, N'Select * from main_table', CAST(N'2019-06-11' AS Date), 1)
INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) VALUES (4, 1, N'Select * From main_table', CAST(N'2019-06-10' AS Date), 1)
INSERT [dbo].[super_user] ([registration_number]) VALUES (1)
SET IDENTITY_INSERT [dbo].[task] ON 

INSERT [dbo].[task] ([task_number], [task_name], [task_topic], [task_type], [task_description], [auxiliary_db_index], [master_query], [task_table]) VALUES (1, N'Знакомство с SELECT', N'Введение', N'0', N'Выведите все поля таблицы main_table', 2, N'SELECT * FROM main_table', N'main_table')
INSERT [dbo].[task] ([task_number], [task_name], [task_topic], [task_type], [task_description], [auxiliary_db_index], [master_query], [task_table]) VALUES (2, N'Вставка данных (INSERT)', N'Введение', N'1', N'Добавьте пользователя с параметрами [ID]=3, [Name] = "Nikolai", [Surname] = "Semenov", [Patronymic] = "Mihailovich" в таблицу main_table', 2, N'insert into main_table ([ID], [Name], [Surname], [Patronymic]) VALUES (3,''Nikolai'',''Semenov'',''Mihailovich'')', N'main_table')
INSERT [dbo].[task] ([task_number], [task_name], [task_topic], [task_type], [task_description], [auxiliary_db_index], [master_query], [task_table]) VALUES (3, N'Вывод данныйх из json', N'NOSQL это просто!', N'0', N'Выведите все поля json из поля таблицы с названием json_col с артиклом товара 0000000001. Поле json_col имеет тип данных nvarchar(MAX).', 1, N'use nosql_market SELECT [key], [value] FROM OPENJSON((SELECT json_col FROM dbo.items WHERE article = ''0000000001''))', NULL)
SET IDENTITY_INSERT [dbo].[task] OFF
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([registration_number], [surname], [name], [patronymic], [login], [passwd]) VALUES (1, N'', N'admin', N'', N'admin', N'1234')
INSERT [dbo].[user] ([registration_number], [surname], [name], [patronymic], [login], [passwd]) VALUES (2, N'Петров', N'Виктор', N'Андреевич', N'Petrov', N'5678')
INSERT [dbo].[user] ([registration_number], [surname], [name], [patronymic], [login], [passwd]) VALUES (4, N'Либанов', N'Никита', N'Игоревич', N'Liblan', N'qwerty')
SET IDENTITY_INSERT [dbo].[user] OFF
ALTER TABLE [dbo].[completed_task]  WITH CHECK ADD  CONSTRAINT [FK_task_completed_task] FOREIGN KEY([task_number])
REFERENCES [dbo].[task] ([task_number])
GO
ALTER TABLE [dbo].[completed_task] CHECK CONSTRAINT [FK_task_completed_task]
GO
ALTER TABLE [dbo].[completed_task]  WITH CHECK ADD  CONSTRAINT [FK_user_completed_task] FOREIGN KEY([registration_number])
REFERENCES [dbo].[user] ([registration_number])
GO
ALTER TABLE [dbo].[completed_task] CHECK CONSTRAINT [FK_user_completed_task]
GO
ALTER TABLE [dbo].[super_user]  WITH CHECK ADD  CONSTRAINT [FK_user_super_user] FOREIGN KEY([registration_number])
REFERENCES [dbo].[user] ([registration_number])
GO
ALTER TABLE [dbo].[super_user] CHECK CONSTRAINT [FK_user_super_user]
GO
ALTER TABLE [dbo].[task]  WITH CHECK ADD  CONSTRAINT [FK_task_auxiliary_db] FOREIGN KEY([auxiliary_db_index])
REFERENCES [dbo].[auxiliary_db] ([auxiliary_db_index])
GO
ALTER TABLE [dbo].[task] CHECK CONSTRAINT [FK_task_auxiliary_db]
GO
/****** Object:  StoredProcedure [dbo].[AutoSelectCompareProc]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AutoSelectCompareProc]
	@RegistrationNumber int,
	@TaskNumber int,
	@SlaveQuery nvarchar(MAX)
AS
BEGIN
	-- Preparing
	IF OBJECT_ID('TmpMasterTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpMasterTable
	END
	IF OBJECT_ID('TmpSlaveTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpSlaveTable
	END
	DECLARE
	@CheckQuery nvarchar(MAX),
	@MasterQuery nvarchar(MAX),
	@ComperableTable nvarchar(MAX),
	@ComperableDataBase nvarchar(MAX),
	@CreateComperableTableString nvarchar(MAX)
	SET @CheckQuery = @SlaveQuery
	SET @MasterQuery = (SELECT [master_query] FROM [dbo].[task] WHERE [task_number] = @TaskNumber)
	SET @ComperableTable = (SELECT [task_table] FROM [dbo].[task] WHERE [task_number] = @TaskNumber)
	SET @ComperableDataBase = (SELECT [reference] FROM [dbo].[auxiliary_db] WHERE [auxiliary_db_index] = (SELECT [auxiliary_db_index] FROM [dbo].[task] WHERE [task_number] = @TaskNumber))
	
	EXEC CreateTableStringGenerator  @ComperableTable, @ComperableDataBase, @CreateComperableTableString OUTPUT

	IF (SELECT [task_type] FROM [dbo].[task] WHERE [task_number] = @TaskNumber) % 2 != 0
	BEGIN				
		EXEC
		('
			USE ' + @ComperableDataBase +
			' BEGIN TRAN TR1 ' +
			'DECLARE @tmp TABLE (' + @CreateComperableTableString + ') ' 
			+ @MasterQuery + 
			'INSERT INTO @tmp SELECT * from ' + @ComperableTable + '
			ROLLBACK TRAN TR1
			USE sim4sql
			SELECT * INTO TmpMasterTable FROM @tmp
		')
		EXEC
		('
			USE ' + @ComperableDataBase +
			' BEGIN TRAN TR1 ' +
			'DECLARE @tmp TABLE (' + @CreateComperableTableString + ') ' 
			+ @SlaveQuery + 
			'INSERT INTO @tmp SELECT * from ' + @ComperableTable + '
			ROLLBACK TRAN TR1
			USE sim4sql
			SELECT * INTO TmpSlaveTable FROM @tmp
		')
		--SELECT * FROM TmpMasterTable
		--SELECT * FROM TmpSlaveTable
		SET @MasterQuery =  N'SELECT * FROM TmpMasterTable'
		SET @CheckQuery = N'SELECT * FROM TmpSlaveTable'
		SET @ComperableDataBase = N'sim4sql'
	END
	-- Autoselect parts
	EXEC ManualSelectCompareProc @MasterQuery, @CheckQuery, @SlaveQuery, @ComperableDataBase, @RegistrationNumber, @TaskNumber
	IF OBJECT_ID('TmpMasterTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpMasterTable
	END
	IF OBJECT_ID('TmpSlaveTable') IS NOT NULL
	BEGIN
		DROP TABLE TmpSlaveTable
	END
END;

GO
/****** Object:  StoredProcedure [dbo].[CreateTableStringGenerator]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CreateTableStringGenerator]
	@TableName nvarchar(MAX),
	@DataBase nvarchar(MAX),
	@OutputString nvarchar(MAX) OUTPUT
AS
BEGIN
	DECLARE
		@ExecuteString nvarchar(MAX),
		@ExecuteOutput nvarchar(MAX),
		@OputString nvarchar(MAX),
		@ParamsCount int,
		@LoopCounter int
	SET @LoopCounter = 1
	SET @OutputString = N''

	SET @ExecuteString = 'SET @tmp = (SELECT count(c.name) FROM [' + @DataBase + '].[sys].[columns] c WITH(NOLOCK) WHERE c.[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U''))'
	EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp tinyint OUTPUT', @tmp = @ParamsCount OUTPUT;

	WHILE @LoopCounter <= @ParamsCount
	BEGIN
		SET @ExecuteString = 'SET @tmp = (SELECT [name] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
		EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
		SET @OutputString = @OutputString + '[' + @ExecuteOutput + '] '
		SET @ExecuteString = 'SET @tmp = (SELECT [type_name] from (SELECT rank() OVER (ORDER BY [c].[name], [tp].[name]) as [id], [c].[name] , [type_name] = [tp].[name] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) JOIN [' + @DataBase + '].[sys].[types] [tp] WITH(NOLOCK) ON [c].[user_type_id] = [tp].[user_type_id] JOIN [' + @DataBase + '].[sys].[schemas] [s] WITH(NOLOCK) ON [tp].[schema_id] = [s].[schema_id] WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) as [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
		EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
		SET @OutputString = @OutputString +'[' + @ExecuteOutput + ']'
		IF @ExecuteOutput = 'nchar' OR @ExecuteOutput = 'nvarchar' OR @ExecuteOutput = 'char' OR @ExecuteOutput = 'varchar'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [max_length] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[max_length] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			IF Convert(int, @ExecuteOutput) > 0
				SET @ExecuteOutput = Convert(int, @ExecuteOutput) / 2
			ELSE
				SET @ExecuteOutput = 'MAX'
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'binary' OR @ExecuteOutput = 'varbinary'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [max_length] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[max_length] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			IF Convert(int, @ExecuteOutput) < 0
				SET @ExecuteOutput = 'MAX'
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'datetime2' OR @ExecuteOutput = 'datetimeoffset' OR @ExecuteOutput = 'time'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [scale] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[scale] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ')'
		END
		ELSE IF @ExecuteOutput = 'numeric' OR @ExecuteOutput = 'decimal'
		BEGIN
			SET @ExecuteString = 'SET @tmp = (SELECT [precision] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[precision] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + '(' + @ExecuteOutput + ', '

			SET @ExecuteString = 'SET @tmp = (SELECT [scale] from (SELECT rank() OVER (ORDER BY [c].[name]) as [id], [c].[name], c.[scale] FROM [' + @DataBase + '].[sys].[columns] [c] WITH(NOLOCK) WHERE [c].[object_id] = OBJECT_ID(''[' + @DataBase + '].[dbo].[' + @TableName + ']'', ''U'')) AS [TmpTable] WHERE [id] = ' + Convert(nvarchar(MAX), @LoopCounter) + ')'
			EXEC [master].[dbo].[sp_executesql] @ExecuteString, N'@tmp nvarchar(MAX) OUTPUT', @tmp = @ExecuteOutput OUTPUT;
			SET @OutputString = @OutputString + @ExecuteOutput + ')'
		END
		IF @LoopCounter != @ParamsCount
			SET @OutputString = @OutputString + ', '
		SET @LoopCounter = @LoopCounter + 1
	END
	RETURN
END
GO
/****** Object:  StoredProcedure [dbo].[ManualSelectCompareProc]    Script Date: 14.06.2019 3:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ManualSelectCompareProc] 
	@MasterQuery nvarchar(MAX), 
	@SlaveQuery nvarchar(MAX),
	@UserQuery nvarchar(MAX),
	@ComperableDB nvarchar(MAX),
	@RegistrationNumber int,
	@TaskNumber int
AS
BEGIN
	IF OBJECT_ID('tempdb..#tmpTab') IS NOT NULL
	BEGIN
		DROP TABLE #tmpTab
	END

	CREATE TABLE #tmpTab
	(
		[ErrorCode] [int] NOT NULL
	);

	BEGIN TRY
		EXEC	
		('	USE ' + @ComperableDB + '
			INSERT #tmpTab ([ErrorCode]) VALUES
			(
				(
					SELECT count(*) AS ErrorState 
					FROM ((' + @MasterQuery + ' EXCEPT ' + @SlaveQuery + ') UNION ALL (' + @SlaveQuery + ' EXCEPT ' + @MasterQuery + ')) AS CompareTable
				)
			)
		')
		IF (Select ErrorCode from #tmpTab) = 0
		BEGIN
			DELETE FROM [dbo].[completed_task]
			WHERE [registration_number] =  @RegistrationNumber  AND [task_number] =  + @TaskNumber 
			INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) 
			VALUES ( @RegistrationNumber, @TaskNumber, @UserQuery, GETDATE(), 1)
		END
		ELSE 
		THROW 1, 'Compare failure', 1;  
	END TRY
	BEGIN CATCH
		INSERT #tmpTab ([ErrorCode]) VALUES (1)
		IF (SELECT [result_achievement] FROM [dbo].[completed_task] WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber) = 0
		OR (SELECT [result_achievement] FROM [dbo].[completed_task] WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber) = NULL 
		BEGIN
			DELETE FROM [dbo].[completed_task]
			WHERE [registration_number] = @RegistrationNumber AND [task_number] = @TaskNumber
			INSERT [dbo].[completed_task] ([registration_number], [task_number], [user_query], [date], [result_achievement]) 
			VALUES (@RegistrationNumber, @TaskNumber, @UserQuery, GETDATE(), 0)
		END
	END CATCH
	Select TOP(1) MAX(ErrorCode) from #tmpTab
END
GO
USE [master]
GO
ALTER DATABASE [sim4sql] SET  READ_WRITE 
GO
