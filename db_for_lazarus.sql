USE [master]
GO
IF db_id('db_for_lazarus') is not null
DROP DATABASE [db_for_lazarus]
GO
/****** Object:  Database [db_for_lazarus]    Script Date: 27.05.2019 3:13:09 ******/
CREATE DATABASE [db_for_lazarus]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_for_lazarus', FILENAME = N'B:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\db_for_lazarus.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_for_lazarus_log', FILENAME = N'B:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\db_for_lazarus_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [db_for_lazarus] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_for_lazarus].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_for_lazarus] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_for_lazarus] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_for_lazarus] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_for_lazarus] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_for_lazarus] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_for_lazarus] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_for_lazarus] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_for_lazarus] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_for_lazarus] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_for_lazarus] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_for_lazarus] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_for_lazarus] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_for_lazarus] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_for_lazarus] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_for_lazarus] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_for_lazarus] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_for_lazarus] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_for_lazarus] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_for_lazarus] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_for_lazarus] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_for_lazarus] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_for_lazarus] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_for_lazarus] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_for_lazarus] SET  MULTI_USER 
GO
ALTER DATABASE [db_for_lazarus] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_for_lazarus] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_for_lazarus] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_for_lazarus] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_for_lazarus] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_for_lazarus] SET QUERY_STORE = OFF
GO
USE [db_for_lazarus]
GO
/****** Object:  Table [dbo].[main_table]    Script Date: 27.05.2019 3:13:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[main_table](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Patronymic] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_main_table] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--SET IDENTITY_INSERT [dbo].[main_table] ON 

INSERT [dbo].[main_table] ([ID], [Name], [Surname], [Patronymic]) VALUES (1, N'Ivan', N'Ivanov', N'Ivanovich')
INSERT [dbo].[main_table] ([ID], [Name], [Surname], [Patronymic]) VALUES (2, N'Petr', N'Petrov', N'Petrovich')
--SET IDENTITY_INSERT [dbo].[main_table] OFF
USE [master]
GO
ALTER DATABASE [db_for_lazarus] SET  READ_WRITE 
GO
